@extends('layouts.admin')


@section('content')


    <div class="main-panel">
    @include('layouts.header')

        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Users</h4>
                        </div>


                            <div class="card-body">


                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                        {{--    <table class="table table table-bordered table-striped" width="200">--}}

                                        <td>User_ID</td>
                                        <td>Name</td>
                                        <td>Email</td>
                                        <td>Username</td>
                                        <td>Phone</td>
                                        <td>Gender</td>

                                        </thead>


                                        @foreach($users as $user)

                                  

                                            <tbody>
                                            <tr>
                                                <td>{{ $user->id }}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->username}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td>{{$user->gender}}</td>

                                                <td>
                                                    <button type="button"
                                                            data-product_id="{{ $user->id }}"
                                                            class="btn btn-xs btn-default btn-flat"
                                                            data-toggle="modal" data-target="#edit">
                                                        <i class="fa fa-edit"></i>
                                                    </button>

                                                    <button type="button"
                                                            data-product_id="{{ $user->id }}"
                                                            class="btn btn-xs btn-default btn-flat"
                                                            data-toggle="modal" data-target="#Delete">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>




                                            </tr>
                                            </tbody>


                                        @endforeach

                                    </table>
                                </div>

                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>





    <script src="{{asset('../assets/img/logo-small.png../assets/js/core/jquery.min.js')}}"}></script>
    <script src="{{asset('../assets/js/core/popper.min.js')}}"></script>
    <script src="{{asset('../assets/js/core/bootstrap.min.js')}}"></script>
    <script src="{{asset('../assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
    <!--  Google Maps Plugin    -->
    <script src="{{asset('https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE')}}"></script>
    <!-- Chart JS -->
    <script src="{{asset('../assets/js/plugins/chartjs.min.js')}}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{asset('../assets/js/plugins/bootstrap-notify.js')}}"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{asset('../assets/js/paper-dashboard.min.js?v=2.0.0')}}" type="text/javascript"></script>
    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{asset('../assets/demo/demo.js')}}"></script>




@endsection



