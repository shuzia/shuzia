<!DOCTYPE html>

<html>
<head>
    <title>Shuzia-dashboard Login</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    @csrf

    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{url('style.css')}}">

</head>
<body>

<div class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                        <div class="col-md-9 col-lg-8 mx-auto">

                                <div class = "col-2 p-5">
                                    <image src="{{asset('../assets/img/logo.png')}}" width = "70" height = "70" class="rounded-circle"></image>
                                </div>
                                   <h3 class="login-heading mb-4">Shuzia-dashboard Login</h3>
                        </div>


                            <div class="card-body">

                                <form action="{{url('api/login')}}" method="POST" id="logForm">

                                    {{ csrf_field() }}

                                    <div class="form-label-group">
                                        <div class="form-group row">
                                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >
                                        </div>

                                    </div>

                                    <div class="form-label-group">
                                        <div class="form-group row">
                                            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                                        </div>

                                    </div>

                                    <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign In</button>
                                    <div class="text-center">No account?
                                        <a class="small" href="{{url('api/register')}}">Sign Up</a></div>

                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</div>



</body>
</html>