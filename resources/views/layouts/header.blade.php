<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">

            <div class="col-5 pl-5 ">
                <image src="{{asset('../assets/img/logo.png')}}" width = "120" height = "50" class="rounded-circle"></image>
            </div>
            <a class="navbar-brand" ><strong>Shuzia</strong></a>
        </div>



    </div>
    <div class="top-right links">
        @auth
            <a href="{{ url('api/users') }}">Home</a>
        @else
            <a href="{{ url('api/login') }}">Logout</a>

               @endauth
    </div>


</nav>
