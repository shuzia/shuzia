<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>
        Shuzia
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href=" {{ asset('../assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{asset('../assets/css/paper-dashboard.css?v=2.0.0')}}" rel="stylesheet" />

</head>

<body>



<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">

        <div class="logo">


        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="active ">
                    <a href="{{url('api/users')}}">
                        <i class="nc-icon nc-single-02"></i>
                        <p>All Users</p>
                    </a>
                </li>
                <li>
                     <a href= "{{url('api/testimonies')}}">
                          <i class="nc-icon nc-diamond"></i>
                          <p>Testimonies</p>
                     </a>
                 </li>
            </ul>

        </div>
    </div>



    @yield('content')


</div>


</body>

</html>
