<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', 'ShuziaApiController@get_all_Users')->name('users');


Route::get('/testimonies', 'ShuziaApiController@get_all_Testimonies')->name('testimonies');

// route to show the login form
Route::get('/login', 'ShuziaApiController@showLogin')->name('display');

// route to process the form
Route::post('/login', 'ShuziaApiController@login')->name('login');

//display register form
Route::get('/register', 'ShuziaApiController@showRegister')->name('displayRegister');

Route::post('/register', 'ShuziaApiController@register')->name('register');
