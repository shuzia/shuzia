<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\Console\Input\Input;

class ShuziaApiController extends Controller
{


    public function showLogin()
    {
        // show the form
       return view ('Testimonies.login');
    }

    public function login(Request $request)
    {

        $payload =[
            "email" => $request["email"],
            "password" => $request["password"],
            "submit"=> 'Login'
        ];
        $headers = array('Content-Type' => 'application/json');


        $httpClient = new Client();

// Send a post to the base url
        $response = $httpClient->post( 'https://api-test.shuzia.com/schema/v2/auth/login', [
            'json'    => $payload,
            'headers' => $headers

        ]);
        $body = $response->getBody()->getContents();
        $login = json_decode($body,$assoc = true);

        return redirect('api/users');

    }

    public function showRegister()
    {
        // show the signup form
        return view ('Testimonies.register');
    }

    public  function register(Request $request)
    {
        $payload =[
            "name" => $request["name"],
            "email" => $request["email"],
            "username" => $request["username"],
            "password" => $request["password"],
            "gender"   => $request["gender"],
            "submit"=> 'signup'
        ];
        $headers = array('Content-Type' => 'application/json');


        $httpClient = new Client();


        $response = $httpClient->post( 'https://api-test.shuzia.com/schema/v2/auth/signup', [
            'json'    => $payload,
            'headers' => $headers

        ]);
        $body = $response->getBody()->getContents();
        $signup = json_decode($body,$assoc = true);

        return redirect('api/login');


    }

   public function get_all_Users()
    {
        $client = new Client([
            'headers' =>
                [
                    'Content-Type' => 'application/json',
                    'authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLXRlc3Quc2h1emlhLmNvbVwvc2NoZW1hXC92MlwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NzkzMzQyNzYsImV4cCI6MTU4MDE5ODI3NiwibmJmIjoxNTc5MzM0Mjc2LCJqdGkiOiJ4ekIyOFZ2Y25sbGZ4TWpnIiwic3ViIjozMTc0LCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.67PQvL1SPSfotwjnEABkr6UZS7mlj3QM8CykP9HQwYY'

                ]

        ]);

        $pageStart = 1;
        $perPage = 10;




        $response = $client->get('https://api-test.shuzia.com/schema/v2/users',
            [
                'body' => json_encode(
                    [
                        "results" =>10,
                        "offset" =>  ($pageStart * $perPage) - $perPage,
                        "qu" => 1,
                        "page" =>$pageStart,
                        "sortField" => "id",
                        "sortOrder" => "asc",
                        "q" => ""
                    ]
                )]
        );

        $data = json_decode($response->getBody()->getContents());

        $info = [];
        $info ['title'] = 'All Users';
        $info['users'] = $data->result->data;


        return view('Testimonies.home', $info);

    }

    public function get_all_Testimonies()
    {
        $client = new Client([
            'headers' =>
                [
                    'Content-Type' => 'application/json',

                ]

        ]);

        $response = $client->get('https://api-test.shuzia.com/schema/v2/testimonies',
            [
                'body' => json_encode(
                    [
                        "results" => 5,
                        "page" => "",
                        "sortField" => "id",
                        "sortOrder" => "",
                        "approved" => "",
                        "type" => "",
                        "user" => ""
                    ]
                )]
        );
        $data = json_decode($response->getBody()->getContents());
        $info = [];

        $info['testimonies'] = $data->result->data;

        return view('Testimonies.allTestimonies', $info);

    }

//    public function get_single_testimony($id)
//    {
//        $user = User::latest('created_at')->first()->token;
//        if(!$user){
//            return response([
//                'status'->true,
//                'message' => "user not found"
//            ],200)
//        }
//
//        $client = new Client
//
//        $response = $client->request('GET', 'https://api-test.shuzia.com/schema/v2/testimonies/' .$id, [
//            'headers' => [
//                'Authorization' => 'Bearer '.$token,
//                'Accept' => 'application/json',
//            ],
//        ]);
//
//
//
//
//
//
//        $data = json_decode($response->getBody()->getContents());
//        $info = [];
//
//        $info['testimonies'] = $data->result->data;
//
//        return view('Testimonies.allTestimonies', $info);
//    }

}
